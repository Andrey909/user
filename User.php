<?php


/**
 * Created by PhpStorm.
 * User: Martin
 * Date: 12.12.2017
 * Time: 16:27
 */

class User
{
    public $name;

    /**
     * User constructor.
     * @param $name
     */
    public function __construct($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }
    public $close;

}